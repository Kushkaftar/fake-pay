package handler

import (
	"fake-pay/internal/service"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	service *service.Service
}

func NewHandler(service *service.Service) *Handler {
	return &Handler{
		service: service,
	}
}

func (h *Handler) InitRoutes() *gin.Engine {

	router := gin.New()
	router.Use(cors.Default())

	api := router.Group("/api")
	{
		transaction := api.Group("/transaction")
		{
			transaction.POST("/new", h.newTransaction)
			transaction.POST("/form", h.form)
			transaction.POST("/recurrent", h.recurrent)
			transaction.POST("/refund", h.refund)
			transaction.GET("/status", h.status)
		}
	}

	return router
}
