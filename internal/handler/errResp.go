package handler

import "fake-pay/internal/model"

func errResp(err string) model.Response {
	return model.Response{
		Success: false,
		Errors:  err,
	}
}
