package handler

import (
	"fake-pay/internal/model"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func (h *Handler) recurrent(g *gin.Context) {
	recurrent := model.Recurrent{}

	err := g.BindJSON(&recurrent)
	if err != nil {
		log.Printf("error bind to json - %s", err)
		g.JSON(http.StatusBadRequest, errResp("error bind to json"))
		return
	}

	resp, err := h.service.Recurrent(&recurrent)
	if err != nil {
		log.Printf("some err - %s", err)
		// todo: refactor!!!
		g.JSON(http.StatusBadRequest, errResp("something went wrong"))
		return
	}

	log.Printf("recurrent resp - %+v", resp)

	g.JSON(http.StatusOK, resp)
}
