package handler

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func (h *Handler) status(g *gin.Context) {
	status, ok := g.GetQuery("uuid")
	if !ok {
		g.JSON(http.StatusOK, "not uuid")
	}

	st, err := h.service.Status(status)
	if err != nil {
		log.Printf("some err - %s", err)
		// todo: refactor!!!
		g.JSON(http.StatusBadRequest, errResp("something went wrong"))
		return
	}

	g.JSON(http.StatusOK, st)
}
