package handler

import (
	"fake-pay/internal/model"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func (h *Handler) newTransaction(g *gin.Context) {
	trRequest := model.TransactionRequest{}

	err := g.BindJSON(&trRequest)
	if err != nil {
		log.Printf("error bind to json - %s", err)
		g.JSON(http.StatusBadRequest, errResp("error bind to json"))
		return
	}

	resp, err := h.service.NewTransaction(&trRequest)
	if err != nil {
		log.Printf("some err - %s", err)
		// todo: refactor!!!
		g.JSON(http.StatusBadRequest, errResp("something went wrong"))
		return
	}

	g.JSON(http.StatusOK, resp)
}
