package server

import (
	"fmt"
	"net/http"
)

type Server struct {
	httpServer *http.Server
}

func (s *Server) Start(port string, handler http.Handler) error {
	bindAddr := fmt.Sprintf(":%s", port)
	s.httpServer = &http.Server{
		Addr:    bindAddr,
		Handler: handler,
	}

	return s.httpServer.ListenAndServe()
}
