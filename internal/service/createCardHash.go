package service

import (
	"crypto/md5"
	"fake-pay/internal/model"
	"fmt"
	"time"
)

func createCardHash(card *model.Card) {
	now := time.Now()
	byteCard := []byte(fmt.Sprintf("%d %s %d %d %d %q",
		card.PAN,
		card.CardHolder,
		card.ExpMonth,
		card.ExpYear,
		card.CVC,
		now))

	hashCard := md5.Sum(byteCard)

	card.HashCard = fmt.Sprintf("%x", hashCard)
}
