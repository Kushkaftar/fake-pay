package service

import (
	"database/sql"
	"fake-pay/internal/model"
	"log"
)

func (s *Srv) Status(uuid string) (*model.Response, error) {
	resp := model.Response{}

	st, err := s.db.GetTransaction(uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			resp.Success = false
			resp.Errors = "transaction not found"
			return &resp, nil
		}
		return nil, err
	}
	log.Printf("status - %+v", st)

	resp.Success = true
	resp.TransactionStatus = getStatusStr(st.Status)
	return &resp, nil
}
