package service

import (
	"fake-pay/internal/model"
	"fake-pay/pkg/db_postgres"
)

type Srv struct {
	db *db_postgres.DBMethods
}

func newSrv(db *db_postgres.DBMethods) *Srv {
	return &Srv{
		db: db,
	}
}

type TransactionSRV interface {
	NewTransaction(newTransaction *model.TransactionRequest) (*model.Response, error)
	Recurrent(recurrent *model.Recurrent) (*model.Response, error)
	Status(uuid string) (*model.Response, error)
}

type Service struct {
	TransactionSRV
}

func NewService(db *db_postgres.DBMethods) *Service {
	return &Service{TransactionSRV: newSrv(db)}
}
