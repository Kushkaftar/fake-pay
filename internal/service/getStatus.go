package service

const (
	newStatusStr        = "new"
	processingStatusStr = "processing"
	deniedStatusStr     = "denied"
	approvedStatusStr   = "approved"
)

func getStatusStr(statusInt int) string {
	var statusStr string
	switch statusInt {
	case newTransactionStatus:
		statusStr = newStatusStr
	case processingTransactionStatus:
		statusStr = processingStatusStr
	case deniedTransactionStatus:
		statusStr = deniedStatusStr
	case approvedTransactionStatus:
		statusStr = approvedStatusStr
	}

	return statusStr
}
