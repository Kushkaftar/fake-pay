package service

import (
	"errors"
	"fake-pay/internal/model"
)

func calculationBalance(transaction model.Transaction, cardBalance float32) (*model.BalanceEvent, error) {
	balanceEvent := model.BalanceEvent{}

	res := cardBalance - transaction.Amount
	if res < 0 {
		return nil, errors.New("insufficient funds")
	}

	balanceEvent.TransactionID = transaction.ID
	balanceEvent.OldBalance = cardBalance
	balanceEvent.Withdrawals = transaction.Amount
	balanceEvent.NewBalance = res

	return &balanceEvent, nil
}
