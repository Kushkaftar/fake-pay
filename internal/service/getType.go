package service

const (
	transactionStr = "transaction"
	recurrentStr   = "recurrent"
	refundStr      = "refund"
)

func getTypeString(trType int) string {
	var t string

	switch trType {
	case transactionType:
		t = transactionStr
	case recurrentType:
		t = recurrentStr
	case refundType:
		t = refundStr
	}

	return t
}
