package service

import (
	"fake-pay/internal/model"
	"fmt"
	"log"
)

func (s *Srv) Recurrent(recurrent *model.Recurrent) (*model.Response, error) {

	transaction, err := s.newTransaction(recurrentType, recurrent.Amount)
	if err != nil {
		log.Fatalf("ERROR add new transaction recurrent - %s", err)
		//todo: обработать ошибку
	}

	resp := model.Response{
		TransactionType: getTypeString(transaction.TransactionType),
		TransactionUUID: transaction.UUID,
	}

	//COMMENT START
	// get card
	cardID, err := s.db.Card.GetCard(recurrent.CardToken)
	if err != nil {

		log.Printf("recurent err get card in db, err - %s", err)
		// todo: обработать ошибку
		resp.Success = false
		resp.Errors = fmt.Sprintf("%s", err)

		return &resp, err
	}

	log.Printf("card id - %d", cardID)

	if cardID == 0 {
		// todo: обработать ошибку? проставить статус транзакции отменена
		resp.Success = false
		resp.Errors = "token not found"
		return &resp, nil
	}

	// set card id
	transaction.CardId = cardID
	err = s.db.Transaction.SetCard(transaction.CardId, transaction.UUID)
	if err != nil {
		return nil, err
	}

	// update transaction status
	transaction.Status = processingTransactionStatus
	err = s.db.Transaction.SetStatus(transaction.Status, transaction.UUID)
	if err != nil {
		return nil, err
	}

	// get balance
	cardBalance, err := s.db.Balance.GetBalance(transaction.CardId)
	if err != nil {
		return nil, err
	}

	log.Printf("recerrent, balance %+v", cardBalance)

	balanceEvent, err := calculationBalance(*transaction, cardBalance.CardBalance)
	if err != nil {
		// todo!!! написать обработку ошибки
		transaction.Status = deniedTransactionStatus
		_ = s.db.Transaction.SetStatus(transaction.Status, transaction.UUID)

		// todo: false???
		resp.Success = false
		resp.TransactionStatus = getStatusStr(transaction.Status)

		return &resp, nil
	}

	//update card balance? refactor err??
	//todo: обработать ошибку
	_ = s.db.Balance.UpdateBalance(cardBalance.Id, balanceEvent.NewBalance)
	//log.Printf("error update balance - %s", err)

	// add balance event
	//todo: обработать ошибку
	_, _ = s.db.BalanceEvent.AddBalanceEvent(*balanceEvent)

	// update transaction status
	//todo: обработать ошибку
	transaction.Status = approvedTransactionStatus
	_ = s.db.Transaction.SetStatus(transaction.Status, transaction.UUID)

	resp.Success = true
	resp.TransactionStatus = getStatusStr(transaction.Status)

	return &resp, nil
}
