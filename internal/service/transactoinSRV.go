package service

import (
	"fake-pay/internal/model"
	"log"
)

// transaction status
const (
	_ = iota
	newTransactionStatus
	processingTransactionStatus
	deniedTransactionStatus
	approvedTransactionStatus
)

// transaction type
const (
	_ = iota
	transactionType
	recurrentType
	refundType
)

func (s *Srv) NewTransaction(request *model.TransactionRequest) (*model.Response, error) {
	// todo: refactor

	transaction, err := s.newTransaction(transactionType, request.Amount)
	if err != nil {
		log.Fatalf("ERROR add new transaction - %s", err)
		//todo: обработать ошибку
	}

	resp := model.Response{
		TransactionType: getTypeString(transaction.TransactionType),
		TransactionUUID: transaction.UUID,
	}

	// create hash card and add hash to struct TransactionRequest
	createCardHash(&request.Card)

	// add card to db
	cardId, err := s.db.Card.AddCard(request.Card)
	if err != nil {
		return nil, err
	}

	// set card id
	transaction.CardId = cardId
	err = s.db.Transaction.SetCard(transaction.CardId, transaction.UUID)
	if err != nil {
		return nil, err
	}

	// update transaction status
	transaction.Status = processingTransactionStatus
	err = s.db.Transaction.SetStatus(transaction.Status, transaction.UUID)
	if err != nil {
		return nil, err
	}

	// get balance in card number
	cardBalance := getCardBalanceInPan(request.Card.PAN)

	// add card balance
	// todo нужен ли id баланса
	balanceId, err := s.db.Balance.AddBalance(transaction.CardId, cardBalance)
	if err != nil {
		return nil, err
	}

	balanceEvent, err := calculationBalance(*transaction, cardBalance)
	// if balance - amount > 0 transaction denied
	if err != nil {
		// todo!!! написать обработку ошибки
		transaction.Status = deniedTransactionStatus
		_ = s.db.Transaction.SetStatus(transaction.Status, transaction.UUID)

		// todo: false???
		resp.Success = true
		resp.TransactionStatus = getStatusStr(transaction.Status)
		return &resp, nil
	}

	//update card balance? refactor err??
	err = s.db.Balance.UpdateBalance(balanceId, balanceEvent.NewBalance)
	//log.Printf("error update balance - %s", err)

	// add balance event
	_, _ = s.db.BalanceEvent.AddBalanceEvent(*balanceEvent)

	// update transaction status
	transaction.Status = approvedTransactionStatus
	_ = s.db.Transaction.SetStatus(transaction.Status, transaction.UUID)

	log.Printf("card balance %+v", request)

	resp.Success = true
	resp.TransactionStatus = getStatusStr(transaction.Status)
	resp.TokenCard = request.Card.HashCard

	return &resp, nil
}
