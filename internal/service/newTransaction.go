package service

import (
	"fake-pay/internal/model"
	"github.com/google/uuid"
)

func (s *Srv) newTransaction(transactionType int, amount float32) (*model.Transaction, error) {
	t := model.Transaction{
		UUID:            uuid.NewString(),
		TransactionType: transactionType,
		Status:          newTransactionStatus,
		Amount:          amount,
	}

	id, err := s.db.Transaction.AddNewTransaction(t)
	if err != nil {
		return nil, err
	}

	t.ID = id
	return &t, nil
}
