package model

type Response struct {
	Success           bool   `json:"success"`
	TransactionType   string `json:"transaction_type"`
	TransactionUUID   string `json:"transaction_uuid,omitempty"`
	TransactionStatus string `json:"transaction_status,omitempty"`
	TokenCard         string `json:"token_card,omitempty"`
	Errors            string `json:"errors,omitempty"`
}
