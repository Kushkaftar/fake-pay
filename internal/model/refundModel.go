package model

type Refund struct {
	Amount    float32 `json:"amount"`
	CardToken string  `json:"card_token"`
}
