package model

type Balance struct {
	Id          int     `db:"id"`
	CardId      int     `db:"card_id"`
	CardBalance float32 `db:"card_balance"`
}
