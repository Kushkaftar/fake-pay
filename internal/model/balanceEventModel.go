package model

type BalanceEvent struct {
	ID            int
	TransactionID int
	OldBalance    float32
	Withdrawals   float32
	NewBalance    float32
}
