package model

type TransactionRequest struct {
	TransactionType int     `json:"transaction_type"`
	UUID            string  `json:"uuid"`
	Amount          float32 `json:"amount"`
	Status          int
	Card            Card `json:"card"`
	CardId          int
}
