package model

type Recurrent struct {
	Amount    float32 `json:"amount"`
	CardToken string  `json:"card_token"`
}
