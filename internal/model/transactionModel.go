package model

import "time"

type Transaction struct {
	ID              int       `db:"id"`
	TransactionType int       `db:"transaction_type"`
	UUID            string    `db:"uuid"`
	Amount          float32   `db:"amount"`
	Status          int       `db:"status"`
	CardId          int       `db:"card_id"`
	CreatedAt       time.Time `db:"created_at"`
}
