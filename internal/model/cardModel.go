package model

import (
	"time"
)

type Card struct {
	ID         int       `db:"id"`
	PAN        int       `json:"pan" db:"pan"`
	CardHolder string    `json:"card_holder" db:"card_holder"`
	ExpMonth   int       `json:"exp_month" db:"expMonth"`
	ExpYear    int       `json:"exp_year" db:"expYear"`
	CVC        int       `json:"cvc" db:"cvc"`
	HashCard   string    `db:"hash_card"`
	CreatedAt  time.Time `db:"created_at"`
}
