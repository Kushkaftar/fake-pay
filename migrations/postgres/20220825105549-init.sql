
-- +migrate Up
create table transaction (
    id serial primary key unique not null,
    transaction_type smallint not null,
    status smallint not null,
    uuid varchar (40) unique not null,
    card_id int,
    amount real not null,
    created_at timestamp default current_timestamp
);

create table card (
    id serial primary key unique not null,
    pan bigint not null,
    card_holder varchar (255),
    expMonth int not null,
    expYear int not null,
    cvc int not null,
    hash_card varchar (32) not null,
    created_at timestamp default current_timestamp
);

create table balance (
    id serial primary key unique not null,
    card_id int unique not null,
    card_balance real not null
);

create table balance_event (
    id serial primary key unique not null,
    transaction_id int not null,
    old_balance real not null,
    withdrawals real not null,
    new_balance real not null
);
-- +migrate Down
DROP TABLE transaction;
DROP TABLE card;
DROP TABLE balance;
DROP TABLE balance_event;