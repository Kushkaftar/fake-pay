package db_postgres

import (
	"fake-pay/pkg/config"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func newConnect(c *config.DB) (*sqlx.DB, error) {
	conn, err := sqlx.Open("postgres",
		fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
			c.Host, c.Port, c.User, c.Name, c.Password, c.SSLMode))

	if err != nil {
		return nil, err
	}

	err = conn.Ping()
	if err != nil {
		return nil, err
	}

	err = migration(*c)
	if err != nil {
		return nil, err
	}

	return conn, nil
}
