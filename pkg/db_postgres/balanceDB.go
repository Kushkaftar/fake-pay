package db_postgres

import (
	"fake-pay/internal/model"
	"fmt"
)

const balanceTable = "balance"

func (db *DB) AddBalance(cardId int, balance float32) (int, error) {
	var id int

	query := fmt.Sprintf("INSERT INTO %s (card_id, card_balance) VALUES ($1, $2) RETURNING id",
		balanceTable)

	row := db.db.QueryRow(query,
		cardId,
		balance,
	)

	if err := row.Scan(&id); err != nil {
		return 0, err
	}

	return id, nil
}

func (db *DB) GetBalance(cardId int) (model.Balance, error) {
	balance := model.Balance{}

	query := fmt.Sprintf("SELECT * FROM %s WHERE card_id=$1;", balanceTable)

	err := db.db.QueryRowx(query, cardId).StructScan(&balance)
	if err != nil {
		return balance, err
	}

	return balance, nil
}

// todo: refactor cade id

func (db *DB) UpdateBalance(cardID int, balance float32) error {
	query := fmt.Sprintf("UPDATE %s SET card_balance=$1 WHERE card_id=$2;", balanceTable)
	_, err := db.db.Exec(query, balance, cardID)
	if err != nil {
		return err
	}

	return nil
}
