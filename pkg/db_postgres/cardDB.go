package db_postgres

import (
	"fake-pay/internal/model"
	"fmt"
	"log"
)

const cardTable = "card"

func (db *DB) AddCard(card model.Card) (int, error) {
	var id int

	query := fmt.Sprintf("INSERT INTO %s (pan, card_holder, expMonth, expYear, cvc, hash_card) "+
		"VALUES ($1, $2, $3, $4, $5, $6) RETURNING id", cardTable)

	row := db.db.QueryRow(query,
		card.PAN,
		card.CardHolder,
		card.ExpMonth,
		card.ExpYear,
		card.CVC,
		card.HashCard)

	if err := row.Scan(&id); err != nil {
		return 0, err
	}

	return id, nil
}

func (db *DB) GetCard(token string) (int, error) {
	card := model.Card{}

	query := fmt.Sprintf("SELECT id FROM %s WHERE hash_card=$1", cardTable)

	rows, err := db.db.Query(query, token)
	if err != nil {
		fmt.Println(err)
		return 0, err
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&card.ID)
		if err != nil {
			fmt.Println(err)
		}
		log.Println(card)
	}
	//err = rows.Scan(&card.ID)
	//if err != nil {
	//	fmt.Println(err)
	//	return nil, err
	//}

	log.Printf("card id - %d", card.ID)

	//err := db.db.Get(&card, query, token)
	//if err != nil {
	//	fmt.Println(err)
	//	return nil, err
	//}
	return card.ID, nil
}
