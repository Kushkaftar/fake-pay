package db_postgres

import (
	"fake-pay/internal/model"
	"fmt"
)

const balanceEventTable = "balance_event"

func (db *DB) AddBalanceEvent(event model.BalanceEvent) (int, error) {
	var id int

	query := fmt.Sprintf("INSERT INTO %s (transaction_id, old_balance, withdrawals, new_balance) "+
		"VALUES ($1, $2, $3, $4) RETURNING id", balanceEventTable)

	row := db.db.QueryRow(query,
		event.TransactionID,
		event.OldBalance,
		event.Withdrawals,
		event.NewBalance,
	)

	if err := row.Scan(&id); err != nil {
		return 0, err
	}

	return id, nil
}
