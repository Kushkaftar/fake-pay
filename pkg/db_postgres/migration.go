package db_postgres

import (
	"database/sql"
	"fake-pay/pkg/config"
	"fmt"
	migrate "github.com/rubenv/sql-migrate"
	"log"
)

func migration(c config.DB) error {
	migration := &migrate.FileMigrationSource{
		Dir: "migrations/postgres",
	}
	//todo: переделать
	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		c.Host, c.Port, c.User, c.Name, c.Password, c.SSLMode))
	if err != nil {
		return err
	}
	n, err := migrate.Exec(db, "postgres", migration, migrate.Up)
	log.Printf("migration - %d", n)
	if err != nil {
		return err
	}

	log.Printf("Applied %d migrations!\n", n)
	return nil
}
