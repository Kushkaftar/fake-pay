package db_postgres

import (
	"fake-pay/internal/model"
	"fmt"
	"log"
)

const transactionTable = "transaction"

func (db *DB) AddNewTransaction(newTransaction model.Transaction) (int, error) {
	var id int

	query := fmt.Sprintf("INSERT INTO %s (transaction_type, status, uuid, amount) VALUES ($1, $2, $3, $4) RETURNING id",
		transactionTable)

	row := db.db.QueryRow(query,
		newTransaction.TransactionType,
		newTransaction.Status,
		newTransaction.UUID,
		newTransaction.Amount)

	if err := row.Scan(&id); err != nil {
		return 0, err
	}

	return id, nil
}

func (db *DB) GetTransaction(transactionUUID string) (model.Transaction, error) {
	t := model.Transaction{}

	query := fmt.Sprintf("SELECT status, transaction_type, uuid FROM %s WHERE uuid=$1", transactionTable)

	err := db.db.QueryRowx(query, transactionUUID).StructScan(&t)
	if err != nil {
		return t, err
	}
	log.Printf("status transaction - %+v", t)
	return t, nil
	//TODO implement me
	//panic("implement me")
}

func (db *DB) SetCard(cardId int, uuid string) error {
	query := fmt.Sprintf("UPDATE %s SET card_id=$1 WHERE uuid=$2;", transactionTable)
	_, err := db.db.Exec(query, cardId, uuid)

	if err != nil {
		return err
	}
	return nil
}

func (db *DB) SetStatus(status int, uuid string) error {
	query := fmt.Sprintf("UPDATE %s SET status=$1 WHERE uuid=$2;", transactionTable)
	_, err := db.db.Exec(query, status, uuid)

	if err != nil {
		return err
	}
	return nil
}
