package db_postgres

import (
	"fake-pay/internal/model"
	"fake-pay/pkg/config"
	"github.com/jmoiron/sqlx"
	"log"
)

type DB struct {
	db *sqlx.DB
}

func newDB(db *sqlx.DB) *DB {
	return &DB{
		db: db,
	}
}

type Transaction interface {
	AddNewTransaction(newTransaction model.Transaction) (int, error)
	GetTransaction(transactionUUID string) (model.Transaction, error)
	SetStatus(status int, uuid string) error
	SetCard(cardId int, uuid string) error
}

type Card interface {
	AddCard(card model.Card) (int, error)
	GetCard(token string) (int, error)
}

type Balance interface {
	AddBalance(cardId int, balance float32) (int, error)
	GetBalance(cardId int) (model.Balance, error)
	UpdateBalance(cardID int, balance float32) error
}

type BalanceEvent interface {
	AddBalanceEvent(event model.BalanceEvent) (int, error)
}

type DBMethods struct {
	Transaction
	Card
	Balance
	BalanceEvent
}

func NewDBMethods(c *config.DB) *DBMethods {
	conn, err := newConnect(c)
	if err != nil {
		log.Fatalf("db not running, - %s", err)
	}

	return &DBMethods{
		Transaction:  newDB(conn),
		Card:         newDB(conn),
		Balance:      newDB(conn),
		BalanceEvent: newDB(conn),
	}
}
