package config

type Config struct {
	Serv Serv
	DB   DB
}

type Serv struct {
	Port string
}

type DB struct {
	Host     string
	Port     string
	Name     string
	User     string
	Password string
	SSLMode  string
}
