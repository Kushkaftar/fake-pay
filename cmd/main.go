package main

import (
	"fake-pay/internal/handler"
	"fake-pay/internal/server"
	"fake-pay/internal/service"
	"fake-pay/pkg/config"
	"fake-pay/pkg/db_postgres"
	"flag"
	"fmt"
	"log"
	"os"
)

const configDir = "configs"

func main() {
	// get config flag
	// todo не работает, починить
	configFile := flag.String("cfg", "dev", "-cfg add name config file")
	flag.Parse()

	path := fmt.Sprintf("%s/%s.yaml", configDir, *configFile)
	_, err := os.Stat(path)
	if err != nil {
		log.Fatalf("configuration file not found, error - %s", err)
	}

	// get config
	c := config.NewConfig(*configFile, configDir)

	// run db
	db := db_postgres.NewDBMethods(&c.DB)
	log.Printf("db connect - %v", db)

	// run service
	src := service.NewService(db)

	// run handlers
	h := handler.NewHandler(src)
	hs := h.InitRoutes()

	// get new server struct
	srv := new(server.Server)

	// start server
	err = srv.Start(c.Serv.Port, hs)
	if err != nil {
		log.Fatalf("server not started, error - %s", err)
	}
}
